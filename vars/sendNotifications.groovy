#!/usr/bin/env groovy

/**
 * Send notifications based on build status string
 */
//def call(String buildStatus = 'STARTED', String to = 'tecnologia@zentricx.com') {
  // build status of null means successful
  buildStatus =  buildStatus ?: 'SUCCESSFUL'

  // Default values
  //def colorName = 'RED'
  //def colorCode = '#FF0000'
  //def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  //def subject = "http://${env.SITE_URL}: Escaneo de seguridad"
  //def summary = "${subject} (${env.BUILD_URL})"
  //def details = """<p>${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
    //<p>Check console output at &QUOT;<a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a>&QUOT;</p>"""
  //def jobname = "${env.JOB_NAME}"
  // def tomail
  
   //switch (jobname) {
     case "sis-arcor.apps-webs":
         tomail = "jcarrizo@druida.biz; awisner@arcor.com; mmossier@arcor.com;dfirka@druida.biz"
        break
     //case "sitio_institucional_arcor":
       //  tomail = "jazmin.dominguez@avatarla.com; ssatragno@arcor.com"
        //break
     case "promobagley":
         tomail = "mp@digitalheads.com.ar; sdomenech@arcor.com"
        break
     case "PromoMenthoplus":
         tomail = "mp@digitalheads.com.ar;"
        break
     case "Formis":
         tomail = "jazmin.dominguez@avatarla.com; slogullo@arcor.com;"
        break  
     case "promopoosh":
         tomail = "mp@digitalheads.com.ar;"
        break
     case "promobt":
         tomail = "mp@digitalheads.com.ar;"
        break
     default:
        tomail = "agustin@zentricx.com"
		break
	}
  def fileContents = new File('/tmp/output.log')
  // Override default values based on build status
  if (buildStatus == 'STARTED') {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus == 'SUCCESSFUL') {
    color = 'GREEN'
    colorCode = '#00FF00'
    details = """
	  <p>Estimados<p>
      <p>El sitio ${env.JOB_NAME} fue analizado y no se encontraron riesgos de seguridad.<br/>
      <p>Está listo para las pruebas funcionales, y queda sujeto a aprobación de Arcor para el pase a producción.</p>
      <p>Saludos!</p>
      """
  } else {
    color = 'RED'
    colorCode = '#FF0000'
    if ((fileContents.getText('UTF-8').contains('High')) || (fileContents.getText('UTF-8').contains('Medium'))) {
      details = """
		<p>Estimados,</p>
        <p>El sitio ${env.JOB_NAME} fue analizado y se encontraron riesgos de seguridad que NO impiden su instalación en testing y pero SI su posterior pasaje a producción.</p>
        <p>URL para pruebas: http://${env.SITE_URL}</p>
        <p>La agencia deberá resolver estas vulnerabilidades del código, y volver a someter el sitio a un escaneo de seguridad antes de que puedan realizarse las pruebas funcionales.</p>
        <p>Saludos</p>
        <p>Contenido del analisis...</p>
        <p>${fileContents.readLines().join('<br />')}</p>
		"""
    } else if (fileContents.getText('UTF-8').contains('Low')) {
      details = """
        <p>Estimados,</p>
        <p>El sitio ${env.JOB_NAME} fue analizado y se encontraron algunos riesgos de seguridad de baja gravedad, que NO impiden su instalación en testing y su posterior pasaje a producción. </p>
        <p>El mismo está listo para las pruebas funcionales, y queda sujeto a aprobación de Arcor para el pasaje a producción. </p>
        
        <p>URL para pruebas: http://${env.SITE_URL}</p>
        <p>En caso de que las pruebas sean exitosas y se quiera pasar a producción se deberá adjuntar la aprobación del referente de negocio por Zendesk (https://zentricx.zendesk.com/hc/es) </p>
        <p>Saludos!</p>
        <p>Contenido del analisis...</p>
        <p>${fileContents.readLines().join('<br />')}</p>
      """
      // <p>URL para pruebas: http://dev.<nombre del sitio></p>
      } else{
        details = """
            <p>Estimados<p>
            <p>El sitio ${env.JOB_NAME} fue analizado y no se encontraron riesgos de seguridad.<br/>
            <p>Está listo para las pruebas funcionales, y queda sujeto a aprobación de Arcor para el pase a producción.</p>
            <p>Saludos!</p>
            """
      }
    }
    
  

  // Send notifications
  //slackSend (color: colorCode, message: details)

  // hipchatSend (color: color, notify: true, message: summary)

  //emailext (
     // to: "ltorres@zentricx.com; agustin@zentricx.com",
    //  to: "$tomail ;soporte@zentricx.com; agustin@zentricx.com; gapweb@arcor.com",
      //subject: subject,
      //body: details
    //)
}